package com.wawa.payzen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayzenApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayzenApplication.class, args);
	}

}
